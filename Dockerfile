FROM golang:alpine as builder

WORKDIR /app

COPY src/hello-world.go .

RUN go build hello-world.go

FROM scratch

COPY --from=builder /app/hello-world /usr/bin/

ENTRYPOINT ["hello-world"]